<?php

require "./autoload.php" ;

use PHPUnit\Framework\TestCase;

class NoteTest extends TestCase
{

    public function testNoteTotale()
    {
        // tests à effectuer

        $note1 = new Note(18,1);
        $note2 = new Note(18,1,1);
        $note3 = new Note(15,2,1);
        $note4 = new Note(15,2,-1);

        $this->assertEquals(18,$note1->noteTotale(),"Probleme coeff");
        $this->assertEquals(19,$note2->noteTotale(),"Probleme coeff");
        $this->assertEquals(32,$note3->noteTotale(),"Probleme coeff");

        // On a interdit les bonus négatifs
        $this->assertEquals(30,$note4->noteTotale(),"Probleme coeff");


    }
}
