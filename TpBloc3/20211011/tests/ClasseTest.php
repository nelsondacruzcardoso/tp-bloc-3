<?php
require "./autoload.php" ;


use PHPUnit\Framework\TestCase;

class ClasseTest extends TestCase
{

    public function testVerifiePassagerClandestin()
    {
        $slamOption = new Option("SIO","BTS",18,"SLAM",16) ;
        $sisrOption = new Option("SIO","BTS",16,"SISR",14) ;

        $eleveBejaoui = new Eleve("Bejaoui","Sara",$slamOption) ;
        $eleveLazarevic = new Eleve("Lazarevic","Stephane",$slamOption) ;
        $eleveSanz = new Eleve("Sanz","Daniel",$sisrOption) ;

        $listeEleves = array($eleveBejaoui,$eleveSanz,$eleveLazarevic) ;

        $classeSio2Slam = new Classe($slamOption,$listeEleves) ;

        $this->assertEquals(1,count($classeSio2Slam->listePassagersClandestins()), "Erreur calcul passager clandestin") ;
        $this->assertContains($eleveSanz,$classeSio2Slam->listePassagersClandestins(),"Sanz devrait etre clandestin ..." );


    }
}
