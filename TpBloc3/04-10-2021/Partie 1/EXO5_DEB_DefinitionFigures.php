<?php

class Point {
private $abscisse;
private $ordonnee;


    function __construct($uneAbscisse, $uneOrdonnee)
{
  $this->abscisse = $uneAbscisse ;
  $this->ordonnee = $uneOrdonnee;
}

function getAbscisse(){
  return $this->abscisse ;
}

function getOrdonnee(){
  return $this->ordonnee ;
}

    public function __toString()
    {
        return "L'abscisse du point est : ".$this->getAbscisse()." et son ordonnée : ".$this->getOrdonnee()." <BR>";
    }

}

class Rectangle {
private $pointHautGauche;
private $pointBasDroite;

function __construct($premierPoint, $deuxiemePoint)
{
  $this->pointHautGauche = $premierPoint ;
  $this->pointBasDroite = $deuxiemePoint;
}

function getPointHautGauche() {
  return $this->pointHautGauche ;
}

function getPointBasDroite() {
  return $this->pointBasDroite ;
}
public function __toString(){
    return "Ce rectangle est composé de 2 points : <BR> Le premier est ".$this->getPointHautGauche()." et le second est ".$this->getPointBasDroite();
}

}

class Cercle {
    private $pointCentre;
    private $rayon ;

    function __construct ($point, $rayon){
        $this->pointCentre = $point ;
        $this->rayon = $rayon;
    }


    function getPointCentre(){
        return $this->pointCentre;
    }

    function getRayon(){
        return $this->rayon ;
    }

   // Calcul du périmètre
    function perimetre(){
        return 2 * M_PI * $this->getRayon();
    }



    // Calcul de la surface
    function surface(){
        return $this->getRayon()*$this->getRayon()*M_PI;
    }

}


?>
