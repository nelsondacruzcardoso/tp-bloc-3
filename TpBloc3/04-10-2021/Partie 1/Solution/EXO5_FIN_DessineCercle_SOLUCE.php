<?php
require "EXO5_DEB_DefinitionFigures_SOLUCE.php";

class dessineCercle {

public $blue ;

public $cercles = array() ;

function __construct (... $listeCercle) {
  $this->cercles=$listeCercle ;
}

function dessineMoi() {

  $canvas = imagecreatetruecolor(800, 800) ;
  $blue  = imagecolorallocate($canvas,   0,   0, 255);


foreach ($this->cercles as $cercle)
{
    imagearc($canvas, $cercle->getPointCentre()->getAbscisse(), $cercle->getPointCentre()->getOrdonnee(),$cercle->getRayon(), $cercle->getRayon(), 0,0, $blue);

}

    header('Content-Type: image/png');

    imagepng($canvas);
    imagedestroy($canvas);
}

}

$premierPoint = new Point(100,50) ;
$deuxiemePoint = new Point(300,400) ;

$premierCercle = new Cercle($premierPoint,120) ;
$deuxiemeCercle = new Cercle($deuxiemePoint,310) ;


$monDessin = new dessineCercle($premierCercle, $deuxiemeCercle) ;

$monDessin->dessineMoi() ;

?>
