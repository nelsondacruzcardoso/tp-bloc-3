<?php
abstract class PDOMySQLRepository{
    const USERNAME="Lereal_M";
    const PASSWORD="13M34@";
    const SERVER="localhost";
    const DB="lbessaci";

    private function getConnection(){
        $username = self::USERNAME;
        $password = self::PASSWORD;
        $server = self::SERVER;
        $db = self::DB;
        try {
            $connection = new PDO("mysql:host=$server;dbname=$db", $username, $password);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Échec lors de la connexion : ' . $e->getMessage();
        }
        return $connection;
    }
    protected function queryList($sql, $fetch_mode,$nom_classe){
        $connection = $this->getConnection();
        $stmt = $connection->prepare($sql);
        if ($nom_classe === NULL && $fetch_mode != PDO::FETCH_COLUMN)
        {
            $stmt->setFetchMode($fetch_mode);
        }
        else if ($fetch_mode == PDO::FETCH_COLUMN) {
            $stmt->setFetchMode($fetch_mode,0);

        }
        else
        {
            $stmt->setFetchMode($fetch_mode|PDO::FETCH_PROPS_LATE,$nom_classe);
        }
        $stmt->execute();
        return $stmt;
    }
}

?>
