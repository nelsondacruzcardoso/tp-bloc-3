<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Rectangle</title>

</head>
<body>
<?php
// Inclusion de la classe Rectangle:
require('EXO2_Rectangle.php');

// Definition longueur et largeur:
$longueur = 160;
$largeur = 160;



// Message bienvenue:
echo "<h2>Etude de rectangle de longueur $longueur et largeur $largeur</h2>";

// Création de rectangle:
    $unRectangle = new Rectangle();
// Initialisation du rectangle
    $unRectangle->largeur = $largeur;
    $unRectangle->longueur = $largeur;
// Impression aire.
    echo $unRectangle->recupereSurface()."<BR>";
// Recupere le perimetre.
    echo  $unRectangle->recuperePerimetre();
// Est ce un carre?
echo '<p>Ce rectangle ';
// instruction if à ajouter
if ($unRectangle->estCarre())
{
echo 'est aussi';
} else {
echo " n'est pas";
}
echo ' un carre.</p>';
// Destruction du rectangle:
unset($r);

?>
</body>
</html>
