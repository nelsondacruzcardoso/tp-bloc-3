<?php
class Compte
{
private $montant;
private $nom ;

    /**
     * @param $montant
     */
    public function __construct($unparametreInitialisationAttributMontant,$unNom)
    {
        $this->montant = $unparametreInitialisationAttributMontant;
        $this->nom = $unNom ;
        $this->privilege() ;
    }

    /**
     * @return mixed
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param mixed $montant
     */
    public function setMontant($montant): void
    {
        $this->montant = $montant;
    }

public function virer(int $valeur,Compte $destination)
{

    $this->montant -= $valeur;
    $destination->montant += $valeur;

}

    private function privilege()
    {

        if ($this->nom == "Robert")
            $this->setMontant(5000);

    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.

        return "Le solde est: ".$this->montant ;

    }


}

// Insérez en dessous le code permettant de créer
// une instance de Compte, nommée compteProfesseur

$compteProfesseur = new Compte(100,"Robert") ;

/*$compteProfesseur->setMontant(100)  ;*/
$compteEleve = new Compte(100,"Dibombe") ;
/*$compteEleve->setMontant(100) ;*/
$compteEleve->virer(50,$compteProfesseur) ;

echo $compteProfesseur."<BR>" ;

echo $compteEleve ;
?>
