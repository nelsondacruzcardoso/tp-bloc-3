<?php
class Compte
{
public $montant;
public function virer($valeur, $destination)
{

    $this->montant -= $valeur;
    $destination->montant += $valeur;
}
}

// Insérez en dessous le code permettant de créer
// une instance de Compte, nommée compteProfesseur
// cf instructions suivantes dans EXO1 du TP

$compteProfesseur = new Compte() ;
$compteProfesseur->montant = 100 ;
$compteEleve = new Compte() ;
$compteEleve->montant = 100 ;
$compteEleve->virer(50,$compteProfesseur) ;
echo "Maintenant il reste $compteEleve->montant à mon élève <BR>" ;
echo "Et moi je dispose de $compteProfesseur->montant  <BR>" ;

?>
