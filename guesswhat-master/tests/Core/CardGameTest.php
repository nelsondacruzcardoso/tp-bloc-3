<?php

namespace App\Tests\Core;

use App\Core\Card;
use App\Core\CardGame;
use PHPUnit\Framework\TestCase;

class CardGameTest extends TestCase
{

  public function testToString2Cards()
  {
    $jeudecarte = new CardGame([new Card('As', 'Pique'), new Card('Roi', 'Coeur')]);
    $this->assertEquals('CardGame : 2 carte(s)',$jeudecarte->__toString());
  }

  public function testToString1Card()
  {
    $jeudecarte = new CardGame([new Card('As', 'Pique')]);
    $this->assertEquals('CardGame : 1 carte(s)',$jeudecarte->__toString());
  }

  public function testCompare()
  {
      $c1 = new Card('7','Pique');
      $c2 = new Card('7','Carreau');
      $this->assertEquals(1, CardGame::compare($c1,$c2));

  }

  public function testShuffle()
  {
      $card1 = new  Card('5','Coeur');
      $card2 = new  Card('8','Trefle');
      $card3 = new  Card('Valet','Coeur');
      $card4 = new  Card('As','Pique');

      $jeudecarte = new CardGame([$card1,$card2,$card3,$card4]);
      $this->assertNotEquals($jeudecarte, $jeudecarte->shuffle());
  }

  public function testGetCard()
  {

  }

  public function testFactoryCardGame32()
  {

  }

}
